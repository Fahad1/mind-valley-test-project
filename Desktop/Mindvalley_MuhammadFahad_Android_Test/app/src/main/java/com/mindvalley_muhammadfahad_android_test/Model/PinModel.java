package com.mindvalley_muhammadfahad_android_test.Model;

/**
 * Created by Lenovo on 9/3/2016.
 */
public class PinModel {
    String pinId, pinColor, pinCreatedDate;
    int pinWidth, pinHeight, pinLikes;
    boolean likedByUser;

    public String getPinId() {
        return pinId;
    }

    public void setPinId(String pinId) {
        this.pinId = pinId;
    }

    public String getPinColor() {
        return pinColor;
    }

    public void setPinColor(String pinColor) {
        this.pinColor = pinColor;
    }

    public String getPinCreatedDate() {
        return pinCreatedDate;
    }

    public void setPinCreatedDate(String pinCreatedDate) {
        this.pinCreatedDate = pinCreatedDate;
    }

    public int getPinWidth() {
        return pinWidth;
    }

    public void setPinWidth(int pinWidth) {
        this.pinWidth = pinWidth;
    }

    public int getPinHeight() {
        return pinHeight;
    }

    public void setPinHeight(int pinHeight) {
        this.pinHeight = pinHeight;
    }

    public int getPinLikes() {
        return pinLikes;
    }

    public void setPinLikes(int pinLikes) {
        this.pinLikes = pinLikes;
    }

    public boolean isLikedByUser() {
        return likedByUser;
    }

    public void setLikedByUser(boolean likedByUser) {
        this.likedByUser = likedByUser;
    }
}
