package com.mindvalley_muhammadfahad_android_test.Model;

/**
 * Created by Lenovo on 9/3/2016.
 */
public class CategoriesModel {

    int categoriesId, categoriesPhotoCount;
    String CategoriesTitle;

    public int getCategoriesId() {
        return categoriesId;
    }

    public void setCategoriesId(int categoriesId) {
        this.categoriesId = categoriesId;
    }

    public int getCategoriesPhotoCount() {
        return categoriesPhotoCount;
    }

    public void setCategoriesPhotoCount(int categoriesPhotoCount) {
        this.categoriesPhotoCount = categoriesPhotoCount;
    }

    public String getCategoriesTitle() {
        return CategoriesTitle;
    }

    public void setCategoriesTitle(String categoriesTitle) {
        CategoriesTitle = categoriesTitle;
    }
}
