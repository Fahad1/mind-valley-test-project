package com.mindvalley_muhammadfahad_android_test.Model;

/**
 * Created by Lenovo on 9/3/2016.
 */
public class ProfileImageModel {
    String profileImageSmall, profileImageMedium, profileImageLarge;

    public String getProfileImageSmall() {
        return profileImageSmall;
    }

    public void setProfileImageSmall(String profileImageSmall) {
        this.profileImageSmall = profileImageSmall;
    }

    public String getProfileImageMedium() {
        return profileImageMedium;
    }

    public void setProfileImageMedium(String profileImageMedium) {
        this.profileImageMedium = profileImageMedium;
    }

    public String getProfileImageLarge() {
        return profileImageLarge;
    }

    public void setProfileImageLarge(String profileImageLarge) {
        this.profileImageLarge = profileImageLarge;
    }
}
