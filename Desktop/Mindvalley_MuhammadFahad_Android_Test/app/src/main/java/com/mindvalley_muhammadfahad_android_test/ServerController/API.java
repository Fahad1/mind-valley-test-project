package com.mindvalley_muhammadfahad_android_test.ServerController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;


public interface API {
    //  @Headers("Cache-Control: max-age=640000")
    @GET("raw/wgkJgazE")
    Call<JsonElement> GetData();  /*   @GET("wgkJgazE")
    void GetData(Callback<JsonElement> callback);*/
}