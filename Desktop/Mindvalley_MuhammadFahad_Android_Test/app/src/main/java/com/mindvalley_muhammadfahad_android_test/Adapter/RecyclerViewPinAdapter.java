package com.mindvalley_muhammadfahad_android_test.Adapter;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.mindvalley_muhammadfahad_android_test.Model.MainModel;
import com.mindvalley_muhammadfahad_android_test.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Lenovo on 9/3/2016.
 */

public class RecyclerViewPinAdapter extends RecyclerView.Adapter<RecyclerViewPinAdapter.ViewHolder> {

    Context context;
    List<MainModel> mainModelList;

    public RecyclerViewPinAdapter(Context context, List<MainModel> mainModelList) {
        this.context = context;
        this.mainModelList = mainModelList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_pin, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
            YoYo.with(Techniques.ZoomIn)
                .duration(700)
                .playOn(holder.root);

        holder.tv_UserName.setText(mainModelList.get(position).getUsersModelList().get(position).getName());
        holder.tv_Name.setText("+" + mainModelList.get(position).getUsersModelList().get(position).getUserName());
        int pinLikes = mainModelList.get(position).getPinModelList().get(position).getPinLikes();
        holder.pin_item_likes.setText(String.valueOf(pinLikes));
        Picasso.with(context).load(mainModelList.get(position).getUrlsModelList().get(position).getUrlSmall()).into(holder.pin_item_image);
        Picasso.with(context).load(mainModelList.get(position).getProfileImageModelList().get(position).getProfileImageLarge()).into(holder.imgAuthorPic);
    }

    @Override
    public int getItemCount() {
        Log.v("ListSize", mainModelList.size() + "");
        return mainModelList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView pin_item_image, imgAuthorPic;
        TextView tv_UserName, pin_item_likes, tv_Name;
        CardView card_view;
        ProgressBar progressBar;
        RelativeLayout relativeLayout_imageView;
        View root;

        public ViewHolder(View itemView) {
            super(itemView);
            tv_UserName = (TextView) itemView.findViewById(R.id.tv_UserName);
            tv_Name = (TextView) itemView.findViewById(R.id.tv_Name);
            pin_item_likes = (TextView) itemView.findViewById(R.id.pin_item_likes);
            pin_item_image = (ImageView) itemView.findViewById(R.id.pin_item_image);
            card_view = (CardView) itemView.findViewById(R.id.card_view);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            imgAuthorPic = (ImageView) itemView.findViewById(R.id.imgAuthorPic);
            relativeLayout_imageView = (RelativeLayout) itemView.findViewById(R.id.relativeLayout_imageView);
            root = itemView.getRootView();

            root.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Snackbar snackbar;
            snackbar = Snackbar.make(v, "Hello From Fahad", Snackbar.LENGTH_SHORT);
            View snackBarView = snackbar.getView();

            snackBarView.setBackgroundColor(context.getResources().getColor(R.color.white));
            TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
            snackbar.show();
        }
    }
}
