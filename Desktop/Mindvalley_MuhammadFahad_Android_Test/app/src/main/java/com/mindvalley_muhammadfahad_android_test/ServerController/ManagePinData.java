package com.mindvalley_muhammadfahad_android_test.ServerController;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.mindvalley_muhammadfahad_android_test.Adapter.RecyclerViewPinAdapter;
import com.mindvalley_muhammadfahad_android_test.Model.MainModel;
import com.mindvalley_muhammadfahad_android_test.Model.PinModel;
import com.mindvalley_muhammadfahad_android_test.Model.ProfileImageModel;
import com.mindvalley_muhammadfahad_android_test.Model.UrlsModel;
import com.mindvalley_muhammadfahad_android_test.Model.UsersModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 9/3/2016.
 */
public class ManagePinData {
    JSONArray jsonArray;
    MainModel mainModel;
    List<UsersModel> usersModelList;
    List<PinModel> pinModelList;
    List<ProfileImageModel> profileImageModelList;
    List<UrlsModel> urlsModelList;
    private UsersModel usersModel;
    private PinModel pinModel;
    private ProfileImageModel profileImageModel;
    private UrlsModel urlsModel;
    private ArrayList<MainModel> mainModelsList;
    RecyclerView recyclerView;
    Context context;
    ProgressBar progressBar;
    private String filename;

    public ManagePinData(Context context, JSONArray jsonArray, RecyclerView recyclerView, ProgressBar progressBar) {
        this.jsonArray = jsonArray;
        this.recyclerView = recyclerView;
        this.context = context;
        this.progressBar = progressBar;
    }

    public void setModels() {
        mainModelsList = new ArrayList<>();
        mainModel = new MainModel();
        usersModelList = new ArrayList<>();
        pinModelList = new ArrayList<>();
        profileImageModelList = new ArrayList<>();
        urlsModelList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                setPinModel(jsonObject.getString("id"), jsonObject.getString("color"), jsonObject.getString("created_at"), jsonObject.getInt("width"), jsonObject.getInt("height"),
                        jsonObject.getInt("likes"), jsonObject.getBoolean("liked_by_user"));

                setUserModel(jsonObject.getJSONObject("user"));
                setProfileImageModel(jsonObject.getJSONObject("user").getJSONObject("profile_image"));
                setUrlsModel(jsonObject.getJSONObject("urls"));
                setMainList();
            } catch (Exception e) {
                e.printStackTrace();
                Log.v("Error", e.toString());
            }
        }
        setAdapter(mainModelsList);
    }

    public void saveObject(MainModel model) {
        try {
            File mydir = context.getDir("mydir", Context.MODE_PRIVATE); //Creating an internal dir;
            File fileWithinMyDir = new File(mydir, "myfile"); //Getting a file within the dir.

            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileWithinMyDir)); //Select where you wish to save the file...
            oos.writeObject(model); // write the class as an 'object'
            oos.flush(); // flush the stream to insure all of the information was written to 'save_object.bin'
            oos.close();// close the stream

           // MainModel mainModel = (MainModel) loadSerializedObject(fileWithinMyDir);
           // setAdapter(mainModel);
        } catch (Exception ex) {
            Log.v("SerializationError", ex.getMessage());
            ex.printStackTrace();
        }
    }

    public Object loadSerializedObject(File f) {
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(f));
            Object o = ois.readObject();
            return o;
        } catch (Exception ex) {
            Log.v("DeserializationError", ex.getMessage());
            ex.printStackTrace();
        }
        return null;
    }

    private void setMainList() {
        mainModelsList.add(mainModel);
       // saveObject(mainModel);
    }

    public void setAdapter(List<MainModel> mainModelsList) {
        RecyclerViewPinAdapter adapter = new RecyclerViewPinAdapter(context, mainModelsList);
        recyclerView.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);

    }

    void setUserModel(JSONObject user) {
        try {
            usersModel = new UsersModel();
            usersModel.setUserId(user.getString("id"));
            usersModel.setName(user.getString("name"));
            usersModel.setUserName(user.getString("username"));

            usersModelList.add(usersModel);
            mainModel.setUsersModelList(usersModelList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    void setCategoriesModel() {

    }

    void setLinksModel() {

    }

    void setPinModel(String pinId, String pinColor, String pinCreatedDate, int pinWidth, int pinHeight, int pinLikes, boolean likedByUser) {
        pinModel = new PinModel();
        pinModel.setPinId(pinId);
        pinModel.setPinColor(pinColor);
        pinModel.setPinCreatedDate(pinCreatedDate);
        pinModel.setPinWidth(pinWidth);
        pinModel.setPinHeight(pinHeight);
        pinModel.setPinLikes(pinLikes);
        pinModel.setLikedByUser(likedByUser);

        pinModelList.add(pinModel);
        mainModel.setPinModelList(pinModelList);
    }

    void setProfileImageModel(JSONObject profile_image) {

        profileImageModel = new ProfileImageModel();
        try {
            profileImageModel.setProfileImageSmall(profile_image.getString("small"));
            profileImageModel.setProfileImageMedium(profile_image.getString("medium"));
            profileImageModel.setProfileImageLarge(profile_image.getString("large"));

            profileImageModelList.add(profileImageModel);
            mainModel.setProfileImageModelList(profileImageModelList);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    void setUrlsModel(JSONObject urls) {
        urlsModel = new UrlsModel();
        try {
            urlsModel.setUrlRaw(urls.getString("raw"));
            urlsModel.setUrlFull(urls.getString("full"));
            urlsModel.setUrlRegular(urls.getString("regular"));
            urlsModel.setUrlSmall(urls.getString("small"));
            urlsModel.setUrlThumb(urls.getString("thumb"));

            urlsModelList.add(urlsModel);
            mainModel.setUrlsModelList(urlsModelList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
