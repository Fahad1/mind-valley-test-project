package com.mindvalley_muhammadfahad_android_test.Model;

/**
 * Created by Lenovo on 9/3/2016.
 */
public class LinksModel {
    String linkSelf, linkHtml, linkPhotos, linkLikes;

    public String getLinkSelf() {
        return linkSelf;
    }

    public void setLinkSelf(String linkSelf) {
        this.linkSelf = linkSelf;
    }

    public String getLinkHtml() {
        return linkHtml;
    }

    public void setLinkHtml(String linkHtml) {
        this.linkHtml = linkHtml;
    }

    public String getLinkPhotos() {
        return linkPhotos;
    }

    public void setLinkPhotos(String linkPhotos) {
        this.linkPhotos = linkPhotos;
    }

    public String getLinkLikes() {
        return linkLikes;
    }

    public void setLinkLikes(String linkLikes) {
        this.linkLikes = linkLikes;
    }
}
