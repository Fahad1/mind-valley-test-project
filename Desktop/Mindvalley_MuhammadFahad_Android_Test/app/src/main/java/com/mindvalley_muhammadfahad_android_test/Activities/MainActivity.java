package com.mindvalley_muhammadfahad_android_test.Activities;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.JsonElement;
import com.mindvalley_muhammadfahad_android_test.R;
import com.mindvalley_muhammadfahad_android_test.ServerController.API;
import com.mindvalley_muhammadfahad_android_test.ServerController.Connectivity;
import com.mindvalley_muhammadfahad_android_test.ServerController.ManagePinData;
import com.mindvalley_muhammadfahad_android_test.Utils.Util;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Lenovo on 9/3/2016.
 */
public class MainActivity extends AppCompatActivity {

    RecyclerView recycler_PinCard;
    ProgressBar progressBar;
    private StaggeredGridLayoutManager _staggeredGridLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        setData();
    }

    private void init() {
        recycler_PinCard = (RecyclerView) findViewById(R.id.recycler_PinCard);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        _staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recycler_PinCard.setLayoutManager(_staggeredGridLayoutManager);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snackbar;
                snackbar = Snackbar.make(view, "Hello MindValley", Snackbar.LENGTH_SHORT);
                View snackBarView = snackbar.getView();

                snackBarView.setBackgroundColor(getResources().getColor(R.color.white));
                TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(getResources().getColor(R.color.colorAccent));
                snackbar.show();

            }
        });

        recycler_PinCard.setItemAnimator(new RecyclerView.ItemAnimator() {
            @Override
            public void runPendingAnimations() {

            }

            @Override
            public boolean animateRemove(RecyclerView.ViewHolder holder) {
                return false;
            }

            @Override
            public boolean animateAdd(RecyclerView.ViewHolder holder) {
                return false;
            }

            @Override
            public boolean animateMove(RecyclerView.ViewHolder holder, int fromX, int fromY, int toX, int toY) {
                return false;
            }

            @Override
            public boolean animateChange(RecyclerView.ViewHolder oldHolder, RecyclerView.ViewHolder newHolder, int fromLeft, int fromTop, int toLeft, int toTop) {
                return false;
            }

            @Override
            public void endAnimation(RecyclerView.ViewHolder item) {

            }

            @Override
            public void endAnimations() {

            }

            @Override
            public boolean isRunning() {
                return false;
            }
        });
    }

    void setData() {
        File httpCacheDirectory = new File(Environment.getExternalStorageDirectory(), "MindValleyCache");
        File fileWithinMyDir = new File(httpCacheDirectory, "myfile");
        Log.v("DirectoryLocation", fileWithinMyDir.toString());
        try {
            FileOutputStream out = new FileOutputStream(fileWithinMyDir);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int cacheSize = 10 * 1024 * 1024;
        Cache cache = new Cache(httpCacheDirectory, cacheSize);

        OkHttpClient client = new OkHttpClient.Builder()
                .addNetworkInterceptor(REWRITE_CACHE_CONTROL_INTERCEPTOR)
                .cache(cache).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.AdminPageURL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        API service = retrofit.create(API.class);
        Call<JsonElement> call = service.GetData();
        call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                if (response.isSuccessful()) {
                    Log.v("Response", response.body().toString());
                    JSONArray jsonArray = null;
                    try {
                        Log.v("Response", response.body().toString());
                        jsonArray = new JSONArray(response.body().toString());
                        ManagePinData managePinData = new ManagePinData(getApplicationContext(), jsonArray, recycler_PinCard, progressBar);
                        managePinData.setModels();
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.v("ResponseError", e.toString());
                    }
                } else {
                    Log.v("Response", call.toString());
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.d("Error", t.getMessage());
            }
        });
    }

    Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR = new Interceptor() {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {

            CacheControl.Builder cacheBuilder = new CacheControl.Builder();
            cacheBuilder.maxAge(0, TimeUnit.SECONDS);
            cacheBuilder.maxStale(365, TimeUnit.DAYS);
            CacheControl cacheControl = cacheBuilder.build();

            Request request = chain.request();
            if (Connectivity.isConnected(getApplicationContext())) {
                request = request.newBuilder()
                        .cacheControl(cacheControl)
                        .build();
            }
            okhttp3.Response originalResponse = chain.proceed(request);
            if (Connectivity.isConnected(getApplicationContext())) {
                int maxAge = 60 * 60; // read from cache
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, max-age=" + maxAge)
                        .build();
            } else {
                int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
                return originalResponse.newBuilder()
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                        .build();
            }
        }
    };
}
