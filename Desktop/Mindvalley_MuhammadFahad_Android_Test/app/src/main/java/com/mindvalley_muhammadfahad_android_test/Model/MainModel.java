package com.mindvalley_muhammadfahad_android_test.Model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Lenovo on 9/3/2016.
 */
public class MainModel{
    List<UsersModel> usersModelList;
    List<LinksModel> linksModelList;
    List<CategoriesModel> categoriesModelList;
    List<PinModel> pinModelList;
    List<ProfileImageModel> profileImageModelList;
    List<UrlsModel> urlsModelList;

    public List<UsersModel> getUsersModelList() {
        return usersModelList;
    }

    public void setUsersModelList(List<UsersModel> usersModelList) {
        this.usersModelList = usersModelList;
    }

    public List<LinksModel> getLinksModelList() {
        return linksModelList;
    }

    public void setLinksModelList(List<LinksModel> linksModelList) {
        this.linksModelList = linksModelList;
    }

    public List<CategoriesModel> getCategoriesModelList() {
        return categoriesModelList;
    }

    public void setCategoriesModelList(List<CategoriesModel> categoriesModelList) {
        this.categoriesModelList = categoriesModelList;
    }

    public List<PinModel> getPinModelList() {
        return pinModelList;
    }

    public void setPinModelList(List<PinModel> pinModelList) {
        this.pinModelList = pinModelList;
    }

    public List<ProfileImageModel> getProfileImageModelList() {
        return profileImageModelList;
    }

    public void setProfileImageModelList(List<ProfileImageModel> profileImageModelList) {
        this.profileImageModelList = profileImageModelList;
    }

    public List<UrlsModel> getUrlsModelList() {
        return urlsModelList;
    }

    public void setUrlsModelList(List<UrlsModel> urlsModelList) {
        this.urlsModelList = urlsModelList;
    }
}
